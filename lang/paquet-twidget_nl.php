<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-twidget?lang_cible=nl
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// T
	'twidget_description' => 'Eenvoudig te installeren Twitter widget 
_ Zet een inclusie in je skeletten om de widget te tonen:
_ <code>#INCLURE{fond=inclure/twidget_profile,user=GusLeLapin}</code>
 of <code>#INCLURE{fond=inclure/twidget_search,search=#SPIP,title=\'Suivez twitter\',subject=\'SPIP\'}</code>

De plugin gebruikt een proxy om geen rechtstreeks verzoek van je bezoekers naar Twitter te provoceren en zo de sporen van hun activiteiten verborgen te houden (bescherming van de privacy).',
	'twidget_slogan' => 'Eenvoudig te installeren Twitter widget'
);
